package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/kms"
	"github.com/urfave/cli/v2"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"syscall"
)

func main() {

	fatal := func(err error) {
		//goland:noinspection GoUnhandledErrorResult
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(-1)
	}

	keyId := os.Getenv("SECRYPT_KEY_ID")

	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "vim"
	}

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		fatal(err)
		return
	}

	kmsClient := kms.NewFromConfig(cfg)

	pattern := regexp.MustCompile("\\[SECRYPT\\[.*?\\]\\]")

	unwrap := func(s string) string {
		return s[9 : len(s)-2]
	}

	wrap := func(s string) string {
		return "[SECRYPT[" + s + "]]"
	}

	encrypt := func(s string) string {
		if keyId == "" {
			fatal(fmt.Errorf("SECRYPT_KEY_ID is required"))
		}
		output, err := kmsClient.Encrypt(context.TODO(), &kms.EncryptInput{
			KeyId:     aws.String(keyId),
			Plaintext: []byte(s),
		})
		if err != nil {
			fatal(err)
		}
		s = base64.StdEncoding.EncodeToString(output.CiphertextBlob)
		return s
	}

	decrypt := func(s string) string {
		data, err := base64.StdEncoding.DecodeString(s)
		if err != nil {
			fatal(err)
		}
		output, err := kmsClient.Decrypt(context.TODO(), &kms.DecryptInput{
			KeyId:          aws.String(keyId),
			CiphertextBlob: data,
		})
		if err != nil {
			err = nil
			output, err = kmsClient.Decrypt(context.TODO(), &kms.DecryptInput{
				CiphertextBlob: data,
			})
			if err != nil {
				fatal(err)
			}
		}
		s = string(output.Plaintext)
		return s
	}

	app := &cli.App{
		Name:  "secrypt",
		Usage: "secret encrypt and decrypt using AWS KMS",
		Commands: []*cli.Command{
			{
				Name:  "encrypt",
				Usage: "read file or stdin, encrypt placeholders and write to stdout",
				Action: func(c *cli.Context) error {

					var err error
					var data []byte

					if c.NArg() > 0 {
						data, err = ioutil.ReadFile(c.Args().First())
						if err != nil {
							return err
						}
					} else {
						data, err = ioutil.ReadAll(os.Stdin)
						if err != nil {
							return err
						}
					}

					textOutput := pattern.ReplaceAllStringFunc(string(data), func(s string) string {
						return wrap(encrypt(unwrap(s)))
					})

					_, err = os.Stdout.WriteString(textOutput)
					if err != nil {
						return err
					}

					return nil
				},
			},
			{
				Name:  "decrypt",
				Usage: "read file or stdin, decrypt placeholders and write to stdout",
				Action: func(c *cli.Context) error {

					var err error
					var data []byte

					if c.NArg() > 0 {
						data, err = ioutil.ReadFile(c.Args().First())
						if err != nil {
							return err
						}
					} else {
						data, err = ioutil.ReadAll(os.Stdin)
						if err != nil {
							return err
						}
					}

					textOutput := pattern.ReplaceAllStringFunc(string(data), func(s string) string {
						return decrypt(unwrap(s))
					})

					_, err = os.Stdout.WriteString(textOutput)
					if err != nil {
						return err
					}

					return nil
				},
			},
			{
				Name:  "edit",
				Usage: "read file, decrypt placeholders, open editor, encrypt placeholders and write file",
				Action: func(c *cli.Context) error {

					if c.NArg() != 1 {
						return fmt.Errorf("%s needs a file path", c.Command.Name)
					}

					filePath := c.Args().First()

					inputData, err := ioutil.ReadFile(filePath)
					if err != nil {
						return err
					}

					fileTmp, err := os.CreateTemp(os.TempDir(), c.App.Name+"-*.tmp")
					if err != nil {
						return err
					}
					//goland:noinspection GoUnhandledErrorResult
					defer os.Remove(fileTmp.Name())
					err = fileTmp.Close()
					if err != nil {
						return err
					}

					tmpString := pattern.ReplaceAllStringFunc(string(inputData), func(s string) string {
						return wrap(decrypt(unwrap(s)))
					})

					err = ioutil.WriteFile(fileTmp.Name(), []byte(tmpString), os.ModePerm)
					if err != nil {
						return err
					}

					editorCmd := exec.Command(editor, fileTmp.Name())
					editorCmd.Stdin = os.Stdin
					editorCmd.Stdout = os.Stdout
					editorCmd.Stderr = os.Stderr
					err = editorCmd.Run()
					if err != nil {
						return err
					}

					tmpData, err := ioutil.ReadFile(fileTmp.Name())
					if err != nil {
						return err
					}

					outputString := pattern.ReplaceAllStringFunc(string(tmpData), func(s string) string {
						return wrap(encrypt(unwrap(s)))
					})

					err = ioutil.WriteFile(filePath, []byte(outputString), os.ModePerm)
					if err != nil {
						return err
					}

					return nil
				},
			},
			{
				Name:  "execute",
				Usage: "decrypt env vars and execute command",
				Action: func(c *cli.Context) error {

					if c.NArg() == 0 {
						return fmt.Errorf("%s needs a command", c.Command.Name)
					}

					argv := make([]string, 0)
					for _, arg := range c.Args().Slice() {
						arg = pattern.ReplaceAllStringFunc(arg, func(s string) string {
							return decrypt(unwrap(s))
						})
						argv = append(argv, arg)
					}

					envs := os.Environ()
					for i, _ := range envs {
						envs[i] = pattern.ReplaceAllStringFunc(envs[i], func(s string) string {
							return decrypt(unwrap(s))
						})
					}

					err := syscall.Exec(argv[0], argv, envs)
					if err != nil {
						return err
					}

					return nil
				},
			},
		},
	}

	err = app.Run(os.Args)
	if err != nil {
		fatal(err)
		return
	}
}
